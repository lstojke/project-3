pro2: main.o menu2.o complexNumbers2.o cSet.o
	g++ -o pro2 main.o menu2.o complexNumbers2.o cSet.o

main.o: main.cpp menu2.cpp complexNumbers2.cpp cSet.cpp
	g++ -c main.cpp menu2.cpp complexNumbers2.cpp cSet.cpp

menu2.o: menu2.h complexNumbers2.cpp menu2.cpp cSet.cpp
	g++ -c menu2.cpp complexNumbers2.cpp cSet.cpp

cSet.o: cSet.h cSet.cpp complexNumbers2.cpp
	g++ -c cSet.cpp complexNumbers2.cpp

complexNumbers2.o: complexNumbers2.h complexNumbers2.cpp
	g++ -c complexNumbers2.cpp
