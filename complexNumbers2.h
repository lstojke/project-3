#ifndef COMPLEXNUMBERS_H
#define COMPLEXNUMBERS_H

#include <iostream>
#include "cSet.h"
template <class T>
class cSet;
class cNum
{
    double x,y; //x + iy
public:
    cNum(double=0, double=0);
    //~cNum();
    //Przeciazanie operatorow
    cNum operator +(const cNum n1);
    cNum operator +=(const cNum n1);
    cNum operator -(const cNum n1);
    cNum operator -=(const cNum n1);
    bool operator ==(const cNum n);
    bool operator !=(const cNum n);
    friend std::ostream & operator<< (std::ostream &exit, const cNum n);
    friend std::istream & operator>> (std::istream &open,  cNum &n);
    //friend std::ostream & operator<< (std::ostream &out, const cSet &s);
   // template <class U>
    //friend void menu(cSet<U> s1,cSet<U> s2);
    friend void takeParam(cNum *a,cNum *b);
    friend class element<cNum>;
};
/*class cSet
{
    cNum *head;
public:
    cSet();
    ~cSet();
    cNum* getPt(int n)const; //zwraca wskaznik na wybrany element zbioru
    void add(double x, double y); //dodawanie elementu
    void rmove(double x, double y); // usuwanie elementu
    //przeciazanie operatorow
    cSet operator +(const cSet &s) const&;
    cSet& operator +=(const cSet &s);
    cSet operator *(const cSet &s) const&;
    cSet& operator *=(const cSet &s);
    friend std::ostream & operator<< (std::ostream &out, const cSet &s);
};*/
#endif
