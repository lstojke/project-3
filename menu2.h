#ifndef MENU_H
#define MENU_H

#include <iostream>
#include "complexNumbers2.h"
#include "cSet.h"
template <class T>
void menu(cSet<T> s1, cSet<T> s2);
void show();
int choices();
// Pobieranie danych
template <class T>
T load();
void takeParam(cNum *a, cNum *b);
#endif
