#include <iostream>
#include "complexNumbers2.h"
#include "cSet.h"

using namespace std;
template <class T>
T load()
{
    T x;
    cin >> x;
    while(!cin.good())
    {
        cout << "Blad! Wczytaj poprawna liczbe" << endl;
        cin.clear();
        cin.ignore(1000,'\n');
        cin >> x;
    }
    cin.clear();
    cin.ignore(1000,'\n');

    return x;
}
template int load();
template double load();
template cNum load();


void takeParam(cNum *a, cNum *b)
{
    for (int i=1; i<=2; i++)
        {
            double x,y;
            cout << "Podaj wartosc czesci rzeczywistej " << i << " liczby : ";
            x = load<double>();
            cout << "Podaj wartosc czesci urojonej " << i << " liczby : ";
            y = load<double>();
            if(i==1){
                a->x = x;
                a->y = y;}
            else{
                b->x = x;
                b->y = y;}
        }
}

void show()
{
    system("clear");
        cout << "Kontener na liczby zespolone (bez duplikatow)" << endl;
        cout << "---------------------------------------------" << endl;
        cout << "Wybierz 1 z opcji :" << endl;
        cout << "1  - Dodaj element do zbioru" << endl;
        cout << "2  - Usun wybrany element ze zbioru" << endl;
        cout << "3  - Wyswietl sume 2 zbiorow (A U B)" << endl;
        cout << "4  - Wyswietl iloczyn 2 zbiorow" << endl;
        cout << "5  - Dodaj do wybranego zbioru inny zbior" << endl;
        cout << "6  - Pozostaw czesc wspolna 2 zbiorow w jednym z nich (operator *=)" << endl;
        cout << "7  - Wyswietl zbiory" << endl;
        cout << "8  - Dodaj 2 liczby zespolone" << endl;
        cout << "9  - Odejmij 2 liczby zespolone" << endl;
        cout << "10 - Porownaj 2 liczby zespolone" << endl;
        cout << "0  - Wyjscie" << endl;
}


int choices()
{
    int c;
    c = load<int>();
    while(c != 1 && c != 2 && c != 0)
    {
        cout << "Wybierz 1 ze zbiorow (0 - wroc do glownego menu) : ";
        c = load<int>();
    }
    return c;
}

template <class T>
void menu(cSet<T> s1, cSet<T> s2)
{
    int choice;
    do
    {
        show();
        choice = load<int>();
        switch(choice)
        {
        case 1 :
            {

                cout << "Zbior 1. : " << endl << s1 << endl;
                cout << "Zbior 2. : " << endl << s2 << endl;
                cout << "Wybierz zbior (0 - wroc do glownego menu) : ";
                int c = choices();
                if(c == 0)
                    break;
                T x;
                cout << "Wprowadz dane : " << endl;
                x = load<T>();
                if(c == 1)
                    s1.add(x);
                else
                    s2.add(x);
                cout << "Gotowe. Wcisnij dow. klawisz, aby kontynuowac";
                getchar();
                break;
            }
        case 2:
            {
                cout << "Zbior 1. : " << endl << s1 << endl;
                cout << "Zbior 2. : " << endl << s2 << endl;
                cout << "Wybierz zbior (0 - wroc do glownego menu) : ";
                int c = choices();
                if(c == 0)
                    break;
                else if(c == 1)
                {
                    cout << s1;
                    if(s1.getPt(1) == NULL)
                    {
                        cout << "Wcisnij dow. klawisz, aby kontynuowac";
                        getchar();
                        break;
                    }

                    cout << "Wybierz element do usuniecia(0 - powrot do menu) : ";
                    c = load<int>();
                    if(c == 0)
                        break;
                    element<T> *temp = s1.getPt(c);
                    if(temp == NULL)
                    {
                        cout << "Nie ma elementu o takim indeksie" << endl;
                        cout << "Wcisnij dow. klawisz, aby kontynuowac";
                        getchar();
                    }
                    else
                    {
                        s1.rmove(temp->data);
                    }
                }
                else
                {
                    cout << s2;
                    cout << "Wybierz element do usuniecia(0 - powrot do menu) : ";
                    c = load<int>();
                    element<T> *temp = s2.getPt(c);
                    if(temp == NULL)
                    {
                        cout << "Nie ma elementu o takim indeksie" << endl;
                        cout << "Wcisnij dow. klawisz, aby kontynuowac";
                        getchar();
                    }
                    else
                    {
                        s2.rmove(temp->data);
                    }
                }


                break;
            }
        case 3:
            {
                cout << "Suma 2 zbiorow to : " << endl;
                cout << s1+s2 << endl;
                cout << "Wcisnij dow. klawisz, aby kontynuowac";
                getchar();
                break;
            }
        case 4:
            {
                cout << "Iloczyn 2 zbiorow to : " << endl;
                cout << s1*s2 << endl;
                cout << "Wcisnij dow. klawisz, aby kontynuowac";
                getchar();
                break;
            }
        case 5:
            {
                cout << "Wybierz zbior do ktorego chcesz dodac drugi zbior" << endl;
                cout << "Zbior 1. : " << endl << s1 << endl;
                cout << "Zbior 2. : " << endl << s2 << endl;
                int c = choices();
                if(c == 0)
                    break;
                else if(c == 1)
                    s1+=s2;
                else
                    s2+=s1;
                cout << "Gotowe. Wcisnij dow. klawisz, aby kontynuowac";
                getchar();
                break;
            }
        case 6:
            {
                cout << "Wybierz zbior, w ktorym pozostanie czesc wspolna" << endl;
                cout << "Zbior 1. : " << endl << s1 << endl;
                cout << "Zbior 2. : " << endl << s2 << endl;
                int c = choices();
                if(c == 0)
                    break;
                else if(c == 1)
                    s1*=s2;
                else
                    s2*=s1;
                cout << "Gotowe. Wcisnij dow. klawisz, aby kontynuowac";
                getchar();
                break;
            }
        case 7:
            {
            cout << "Zbior 1. : " << endl << s1 << endl;
            cout << "Zbior 2. : " << endl << s2 << endl;
            cout << "Wcisnij dow. klawisz, aby kontynuowac";
            getchar();
            break;
            }
        case 8:
            {
            cNum a,b;
            takeParam(&a,&b);
            cout << a << " + (" << b << ") = " << a+b << endl;
            cout << "Wcisnij dow. klawisz, aby kontynuowac";
            getchar();
            break;
            }
        case 9:
            {
            cNum a,b;
	        takeParam(&a, &b);
            cout << a << " - (" << b << ") = " << a-b << endl;
            cout << "Wcisnij dow. klawisz, aby kontynuowac";
            getchar();
            break;
            }
        case 10:
            {
            cNum a,b;
	        takeParam(&a, &b);
            if(a==b)
                    cout << a << " = " << b << endl;
            else if(a!=b)
                    cout << a << " != " << b << endl;
            cout << "Wcisnij dow. klawisz, aby kontynuowac";
            getchar();
            break;
            }
        case 0:
            {
                cout << "Czy na pewno chcesz wyjsc (T/N)? ";
                char o;
                cin >> o;
                while (o!='T' && o!='t' && o!='N' && o!='n')
                {
                    cout << endl << "Wpisz T, aby wyjsc lub N, aby pozostac ";
                    cin.clear();
                    cin.ignore(1000,'\n');
                    cin >> o;
                }
                if(o == 'n' || o == 'N')
                    choice = -1;
                break;
            }
        default:
            {
                break;
            }
        }
    }while(choice != 0);
}
template void menu(cSet<int> s1, cSet<int> s2);
template void menu(cSet<double> s1, cSet<double> s2);
template void menu(cSet<cNum> s1, cSet<cNum> s2);
