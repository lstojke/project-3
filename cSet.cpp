#include <iostream>
#include <assert.h>
#include "complexNumbers2.h"
#include "cSet.h"

template <class T>
element<T>::element(T a)
{
	data = a;
}
template element<int>::element(int a);
template element<double>::element(double a);
template element<cNum>::element(cNum a);


template <class T>
cSet<T>::cSet()
{
    head = NULL;
}
template cSet<int>::cSet();
template cSet<double>::cSet();
template cSet<cNum>::cSet();

template <class T>
cSet<T>::~cSet()
{
	element<T> *current = head;
	element<T> *next1;
    while( current )
        {
			next1 = current->next;
            delete current;
			current = next1;
        }
	head = NULL;
	assert(head==NULL); ///TESTY
}
template cSet<int>::~cSet();
template cSet<double>::~cSet();
template cSet<cNum>::~cSet();

template <class T>
element<T>* cSet<T>::getPt(int n)const
{
    if(!head)
        return NULL;
    element<T> *temp = head;
	for(int i=0; i<n; ++i)
	{
		if( i == n-1)
        {
            assert(temp!=NULL);
			return temp;
        }
		else if(temp->next)
			temp = temp->next;
		else
            return NULL;
	}
	return NULL;
}
template element<int>* cSet<int>::getPt(int n)const;
template element<double>* cSet<double>::getPt(int n)const;
template element<cNum>* cSet<cNum>::getPt(int n)const;

template <class T>
void cSet<T>::add(T data)
{
 	element<T> *newNum = new element<T>(data);
 	assert(newNum != NULL); ///TESTY
 	newNum->next = NULL;
 	if(head == NULL)
 	{
 	    head = newNum;
		return;
 	}
 	else
	{
    	 element<T> *temp = head;
    	 while(temp)
    	{
    	    if(temp->data==newNum->data)
    	    {
    	        delete newNum;
    	        return;
    	    }
    	    else if(temp->next == NULL)
    	    {
    	        temp->next=newNum;
    	        return;
    	    }
    	    temp = temp->next;
    	}
	}
}
template void cSet<int>::add(int data);
template void cSet<double>::add(double data);
template void cSet<cNum>::add(cNum data);

template <class T>
void cSet<T>::rmove(T x)
{
    if(!head)
    {
        return;
    }
    else
    {
        element<T> *temp = head;
		element<T> *prev = NULL;
        int i = 1;
        while (temp)
        {
            if(temp->data == x)
            {
                if(temp == head)
                    head = head->next;
                else
                    prev->next = temp->next;
                delete temp;
                return;
            }
			++i;
			prev = temp;
            temp = temp->next;
        }
    }
	return;
}
template void cSet<int>::rmove(int x);
template void cSet<double>::rmove(double x);
template void cSet<cNum>::rmove(cNum x);

template <class T>
cSet<T> cSet<T>::operator +(const cSet<T> &s) const&
{
    cSet<T> sTemp;
    element<T> *temp = s.head;
    while (temp)
    {
        sTemp.add(temp->data);
        temp = temp->next;
    }
    temp = head;
    assert(temp != NULL);   ///TESTY
    while (temp)
    {
        sTemp.add(temp->data);
        temp = temp->next;
    }
    return sTemp;
}
template cSet<int> cSet<int>::operator +(const cSet<int> &s) const&;
template cSet<double> cSet<double>::operator +(const cSet<double> &s) const&;
template cSet<cNum> cSet<cNum>::operator +(const cSet<cNum> &s) const&;


template <class T>
cSet<T>& cSet<T>::operator +=(const cSet<T> &s)
{

    element<T> *temp = s.head;
    while (temp)
    {
        add(temp->data);
        temp = temp->next;
    }
    return *this;
}
template cSet<int>& cSet<int>::operator +=(const cSet<int> &s);
template cSet<double>& cSet<double>::operator +=(const cSet<double> &s);
template cSet<cNum>& cSet<cNum>::operator +=(const cSet<cNum> &s);

template <class T>
cSet<T> cSet<T>::operator *(const cSet<T> &s) const&
{
    cSet<T> sTemp;
    element<T> *temp = s.head;

    while (temp)
    {
        element<T> *temp2 = head;
        while(temp2)
        {
            if(temp->data==temp2->data)
                sTemp.add(temp->data);
            temp2 = temp2->next;
        }
        temp = temp->next;
    }
    return sTemp;
}
template cSet<int> cSet<int>::operator *(const cSet<int> &s) const&;
template cSet<double> cSet<double>::operator *(const cSet<double> &s) const&;
template cSet<cNum> cSet<cNum>::operator *(const cSet<cNum> &s) const&;

template <class T>
cSet<T>& cSet<T>::operator *=(const cSet<T> &s)
{
    element<T> *temp = head;
    while (temp)
    {
		element<T> *temp2 = s.head;
        bool flag = false;
        while(temp2)
        {
            if(temp->data==temp2->data)
                flag = true;
            temp2 = temp2->next;
        }
		element<T> *help = temp;
		temp = temp->next;
        if(!flag)
            rmove(help->data);

    }
    return *this;
}
template cSet<int>& cSet<int>::operator *=(const cSet<int> &s);
template cSet<double>& cSet<double>::operator *=(const cSet<double> &s);
template cSet<cNum>& cSet<cNum>::operator *=(const cSet<cNum> &s);


template <class T>
std::ostream & operator<< (std::ostream &out, const cSet<T> &s)
{
    int i=1;
	element<T> *temp = s.head;

    if(s.head == NULL)
    {
        out << "Ten zbior jest pusty" << std::endl;
        return out;
    }
	while(temp != NULL) {
	    out << i << ". " << temp->data << std::endl;
	  	temp = temp->next;
	    ++i;
	 	}
    return out;
}
template std::ostream & operator<< (std::ostream &out, const cSet<int> &s);
template std::ostream & operator<< (std::ostream &out, const cSet<double> &s);
template std::ostream & operator<< (std::ostream &out, const cSet<cNum> &s);
