#ifndef CSET_H
#define CSET_H

#include <iostream>
//#include "complexNumbers2.h"

template <class T>
class cSet;
template <class T>
class element
{
	T data;
	element *next;
	public:
	//element() : data=0{};
	element(T=0);
	friend class cSet<T>;
	template <class U>
	friend void menu(cSet<U> s1,cSet<U> s2);
    template <class U>
	friend std::ostream & operator<< (std::ostream &out, const cSet<U> &s);
};
template <class T>
class cSet
{
    element<T> *head;
public:
    cSet();
    ~cSet();
    element<T>* getPt(int n)const; //zwraca wskaznik na wybrany element zbioru
    void add(T data); //dodawanie elementu
    void rmove(T x); // usuwanie elementu
    //przeciazanie operatorow
    cSet operator +(const cSet &s) const&;
    cSet& operator +=(const cSet &s);
    cSet operator *(const cSet &s) const&;
    cSet& operator *=(const cSet &s);
    template <class U>
    friend std::ostream & operator<< (std::ostream &out, const cSet<U> &s);
};
#endif
